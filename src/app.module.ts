import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { User } from "./users/users";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
// import { User } from "./users/user";
import { UsersModule } from "./users/users.module";
import { PostsModule } from "./post/post.module";
import { Post } from "./post/post";

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: "postgres",
      host: "localhost",
      port: 5433,
      username: "reviewty",
      password: "123456aA",
      database: "postgres",
      logging: true,
      synchronize: true,
      entities: [User, Post],
    }),
    UsersModule,
    PostsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

import {
    
    Column,
    CreateDateColumn,
    Entity,
    PrimaryColumn,
  } from "typeorm";
import { Person } from "./utils/Person";
  
  @Entity("banker")
  export class Banker extends Person {
    @PrimaryColumn()
    id: number;
  
    @Column()
    first_name: string;
  
    @Column()
    last_name: string;
  
    @Column({
      unique: true,
    })
    email: string;
  
    @Column({
      unique: true,
      length: 10,
    })
    card_number: string;
  
    @Column({
      type: "numeric",
    })
    balance: number;
  
    @Column({
      default: true,
      name: "active",
    })
    is_active: boolean;
  
  
    @Column({
      type: "simple-json",
      nullable: true,
    })
    additional_info: {
      age: number;
      hair_color: string;
    };
  
    @Column({
      type: "simple-array",
    })
    family_memver: string[];
  
    @Column({
      unique: true,
      length:10
    })
    employee_number: string;
  
    @CreateDateColumn()
    create_at: Date;
  
    @CreateDateColumn()
    update_at: Date;
  }
  
import {
  Column,
  CreateDateColumn,
  Entity,
} from "typeorm";
import { Person } from "./utils/Person";

@Entity("client")
export class Client extends Person {

  @Column({
    type: "numeric",
  })
  balance: number;

  @Column({
    default: true,
    name: "active",
  })
  is_active: boolean;

  @Column({
    type: "simple-json",
    nullable: true,
  })
  additional_info: {
    age: number;
    hair_color: string;
  };

  @Column({
    type: "simple-array",
  })
  family_memver: string[];

  @CreateDateColumn()
  create_at: Date;

  @CreateDateColumn()
  update_at: Date;
}

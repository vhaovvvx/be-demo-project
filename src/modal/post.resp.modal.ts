export class postReq {
  id: number;
  userId: number;
  title: string;
  author: string;
  description: string;
  date_posted: string;
}

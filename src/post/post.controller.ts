import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  UsePipes,
  ValidationPipe,
} from "@nestjs/common";
import { postReq } from "src/modal/post.resp.modal";
import { CreatePostDTO } from "./post.dto";
import { PostsService } from "./post.service";

@Controller("posts")
export class PostsController {
  constructor(private postsService: PostsService) {}

  @Get()
  async getPosts() {
    const posts = this.postsService.getPosts();
    return posts;
  }

  @Get(":id")
  async getPost(@Param() id: { id: string }) {
    const posts = this.postsService.getPost(id);
    return posts;
  }

  @Post()
  @UsePipes(ValidationPipe)
  async addPost(@Body() postReq: CreatePostDTO) {
    const post = await this.postsService.addPost(postReq);

    return { msg: "success", ...post };
  }
}

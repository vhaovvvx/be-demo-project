import { IsNotEmpty, isNotEmpty } from "class-validator";

export class CreatePostDTO {
  id: number;
  @IsNotEmpty()
  userId: number;

  @IsNotEmpty()
  title: string;
  author: string;
  description: string;
  date_posted: string;
}

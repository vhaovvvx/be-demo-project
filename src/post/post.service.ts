import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { postReq } from "src/modal/post.resp.modal";
import { Repository } from "typeorm";
import { Post } from "./post";

@Injectable()
export class PostsService {
  [x: string]: any;
  constructor(@InjectRepository(Post) private post: Repository<Post>) {}

  public async getPosts(): Promise<any> {
    return this.post;
  }

  public async getPost(postId: { id: string }): Promise<any> {
    const toNumberId = parseInt(postId.id);

    return this.post.findOne({ id: toNumberId });
  }

  public async addPost(post: postReq): Promise<any> {
    let newPost = new Post();

    newPost.description = post.description;
    newPost.title = post.title;
    newPost.author = post.author;
    newPost.userId = post.userId;
    newPost.date_posted = post.date_posted;

    await this.post.insert(post);
    return {
      successStatus: true,
      message: "success",
      post: newPost,
    };
  }

  public async editPost(postId: string | number): Promise<any> {}

  public async deletePost(postId: string): Promise<any> {
    const id = Number(postId);

    await this.post.delete(id);

    return {
      successStatus: true,
      message: "delete-success",
    };
  }
  //   public async getPosts(): Promise<any> {
  //     return this.posts;
  //   }

  //   public async getPosts(): Promise<any> {
  //     return this.posts;
  //   }
}

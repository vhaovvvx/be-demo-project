import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({ name: "post" })
export class Post {
  @PrimaryGeneratedColumn("increment", { name: "id" })
  id: number;

  @Column({ name: "userId" })
  userId: number;

  @Column({ name: "title" })
  title: string;

  @Column({ name: "author" })
  author: string;

  @Column({ name: "description" })
  description: string;

  @Column({ name: "date_posted" })
  date_posted: string;
}

import {
  BadRequestException,
  Injectable,
  UnauthorizedException,
} from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { Request } from "express";
import { ExtractJwt, Strategy } from "passport-jwt";
import { UsersService } from "./users.service";

@Injectable()
export class RefreshStrategy extends PassportStrategy(Strategy, "refresh") {
  constructor(private userService: UsersService) {
    super({
      usernameField: "email",
      ignoreExpiration: false,
      passReqToCallback: true,
      secretOrKey: "secret",
      jwtFromRequest: ExtractJwt.fromExtractors([
        (request: Request) => {
          let data = request?.cookies["auth-cookie"];
          if (!data) {
            return null;
          }
          return data.token;
        },
      ]),
    });
  }

  async validate(req: Request, payload: any) {
    console.log(payload.email);
    if (!payload) {
      throw new BadRequestException("invalid jwt token");
    }
    let data = req?.cookies["auth-cookie"];
    if (!data?.refreshToken) {
      throw new BadRequestException("invalid refresh token");
    }
    let user = await this.userService.validRefreshToken(
      payload.email,
      data.refreshToken
    );
    if (!user) {
      throw new BadRequestException("token expired");
    }

    return user;
  }
}

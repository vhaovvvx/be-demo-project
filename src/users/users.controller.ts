import {
  Controller,
  Post,
  Body,
  Get,
  UseGuards,
  Request,
  Req,
  Res,
} from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import { response, Response } from "express";
import { CurrentUser } from "src/modal/current.user";
import { registrationReqModel } from "src/modal/registration.req.modal";
import { UsersService } from "./users.service";

@Controller("auth")
export class UsersController {
  constructor(private userService: UsersService) {}

  @Post("registration")
  async registerUser(@Body() reg: registrationReqModel) {
    return await this.userService.registerUser(reg);
  }

  @Post("login")
  @UseGuards(AuthGuard("local"))
  async login(@Req() req: any, @Res({ passthrough: true }) res: Response) {
    const token = await this.userService.getJwtToken(req.user.userId);
    const refreshToken = await this.userService.getRefreshToken(
      req.user.userId
    );

    const secretData = {
      token,
      refreshToken,
    };

    res.cookie("auth-cookie", secretData, { httpOnly: true });

    return { msg: "success", ...req.user, secretData };
  }
  @Get("profile")
  @UseGuards(AuthGuard("jwt"))
  async getProfile(@Request() req: any) {
    return req.user;
  }

  @Get("refresh-tokens")
  @UseGuards(AuthGuard("refresh"))
  async regenerateTokens(
    @Req() req: any,
    @Res({ passthrough: true }) res: Response
  ) {
    const token = await this.userService.getJwtToken(req.user as CurrentUser);
    const refreshToken = await this.userService.getRefreshToken(
      req.user.userId
    );
    const secretData = {
      token,
      refreshToken,
    };

    res.cookie("auth-cookie", secretData, { httpOnly: true });
    return { msg: "success" };
  }
  @Get("logout")
  async logout(@Res({ passthrough: true }) res: Response) {
    try {
      res.clearCookie("auth-cookie");
      return "logout successfully";
    } catch (error) {
      return false;
    }
  }
}

import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { registrationReqModel } from "src/modal/registration.req.modal";
import { MoreThanOrEqual, Repository } from "typeorm";
import { User } from "./users";
import * as bcrypt from "bcrypt";
import { registrationRespModel } from "src/modal/registration.resp.modal";
import { CurrentUser } from "src/modal/current.user";
import { JwtService } from "@nestjs/jwt";
import randomToken from "rand-token";
import moment from "moment";

@Injectable()
export class UsersService {
  [x: string]: any;
  constructor(
    @InjectRepository(User) private user: Repository<User>,
    private jwtService: JwtService
  ) {}

  public async getJwtToken(user: CurrentUser): Promise<any> {
    const payload = { ...user };

    return this.jwtService.signAsync(payload);
  }

  public async getRefresheToken(userId: number): Promise<string> {
    const userDataToUpdate = {
      refreshToken: randomToken.generate(16),
      refresheTokenExp: moment().day(1).format("YYYY/MM/DD"),
    };

    await this.user.update(userId, userDataToUpdate);
    return userDataToUpdate.refreshToken;
  }

  public async getHello(): Promise<any> {
    return "hello";
  }

  private async registrationValidation(
    regModel: registrationReqModel
  ): Promise<string> {
    if (!regModel.email) {
      return "Email can't be emty";
    }

    const emailRegex =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|((?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)+[a-zA-Z]{2,}))$/;

    if (!emailRegex.test(regModel.email.toLowerCase())) {
      return "Invalid email";
    }
    const user = await this.user.findOne({ email: regModel.email });

    if (user != null && user.userId > 0) {
      return "Email already exist";
    }

    if (regModel.password !== regModel.confirmPassword) {
      return "Confirm password not matching";
    }
    return "";
  }

  private async getPasswordHash(password: string): Promise<string> {
    const hash = await bcrypt.hash(password, 10);
    return hash;
  }

  public async registerUser(
    regModel: registrationReqModel
  ): Promise<registrationRespModel> {
    let result = new registrationRespModel();
    const errorMessage = await this.registrationValidation(regModel);

    if (errorMessage) {
      result.message = errorMessage;
      result.successStatus = false;
      return result;
    }

    let newUser = new User();

    newUser.firstName = regModel.firstName;
    newUser.lastName = regModel.lastName;
    newUser.email = regModel.email;
    newUser.password = await this.getPasswordHash(regModel.password);

    await this.user.insert(newUser);

    result.successStatus = true;
    result.message = "success";

    return result;
  }

  public async validateUserCredentials(
    email: string,
    password: string
  ): Promise<CurrentUser | null> {
    let user = await this.user.findOne({ email: email });

    if (user == null) {
      return null;
    }

    const isValidPassword = await bcrypt.compare(password, user.password);
    if (!isValidPassword) {
      return null;
    }

    let currentUser = new CurrentUser();
    currentUser.userId = user.userId;
    currentUser.firstName = user.firstName;
    currentUser.lastName = user.lastName;
    currentUser.email = user.email;

    return currentUser;
  }

  public async validRefreshToken(
    email: string,
    refreshToken: string
  ): Promise<CurrentUser | null> {
    const currentDate = moment().day(1).format("YYYY/MM/DD");
    let user = await this.user.findOne({
      where: {
        email: email,
        refreshToken: refreshToken,
        refreshTokenExp: MoreThanOrEqual(currentDate),
      },
    });

    if (!user) {
      return null;
    }

    let currentUser = new CurrentUser();
    currentUser.userId = user.userId;
    currentUser.firstName = user.firstName;
    currentUser.lastName = user.lastName;
    currentUser.email = user.email;

    return currentUser;
  }

  public async getRefreshToken(userId: number): Promise<string> {
    const userDataToUpdate = {
      refreshToken: randomToken.generate(16),
      refreshTokenExp: moment().day(1).format("YYYY/MM/DD"),
    };

    await this.user.update(userId, userDataToUpdate);
    return userDataToUpdate.refreshToken;
  }
}

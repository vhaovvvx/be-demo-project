import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({ name: "users" })
export class User {
  @PrimaryGeneratedColumn("increment", { name: "userId" })
  userId: number;

  @Column({ name: "firstnname" })
  firstName: string;

  @Column({ name: "lastname" })
  lastName: string;

  @Column({ name: "email" })
  email: string;

  @Column({
    name: "password",
  })
  password: string;

  @Column({ nullable: true, name: "refreshtoken" })
  refreshToken: string;

  @Column({ type: "date", nullable: true, name: "refreshtokenexp" })
  refreshTokenExp: string;
}
